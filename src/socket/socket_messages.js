import { ce, WsEvt } from '../library/storage';
import CryptoJS from 'crypto-js';

function msgSend(evt, payload, params) {
  return {
    msg: {
      event: ce(evt),
      payload,
      params,
    },
    task: 'send',
  };
}

export const logoutMsg = () => {
  return msgSend(WsEvt.Player_Logout, {});
};

export const loginMsg = ({ username, password }) => {
  const payload = {
    cUsername: username,
    cPassword: CryptoJS.MD5(password).toString(),
  };

  return msgSend(WsEvt.Player_Login, payload);
};

export const bigWinner = () => {
  return msgSend(WsEvt.Casino_Game_Winners);
};

export const initConnection = (payload) => {
  return msgSend(WsEvt.Connection_Init, payload);
};
