export const WS_CONNECT = 'WS_CONNECT';
export const WS_RECEIVED = 'WS_RECEIVED';
export const WS_DISCONNECT = 'WS_DISCONNECT';
export const WS_SEND = 'WS_SEND';
export const DELETE_DATA = 'DELETE_DATA';

export const actions = {
    wsConnect: () => ({type: WS_CONNECT}),

    wsReceived: (payload) => ({
        type: WS_RECEIVED,
        payload,
    }),

    wsSend: (payload) => ({
        type: WS_SEND,
        payload,
    }),

    deleteData: (key) => ({
        type: DELETE_DATA,
        key,
    }),
};
