import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';
import TextField from '@mui/material/TextField';
import { actions } from '../socket/actions';
import { loginMsg } from '../socket/socket_messages';
import { makeStyles } from '@material-ui/core';
import Toast from './Toast';

const useStyles = makeStyles(() => ({
  root: {
    height: '100vh',
    display: 'flex',
    placeItems: 'center',
  },
  loginForm: {
    width: 330,
    margin: '0 auto',
    display: 'flex',
    flexDirection: 'column',
    background: 'white',
    padding: 30,
  },
  items: {
    margin: '10px !important',
  },
  inputContainer: {
    margin: '10px 0',
  },
  errorMessage: {
    fontSize: 12,
    color: 'red',
    letterSpacing: 1,
    paddingLeft: 10,
    margin: 3,
  },
}));

const schema = yup.object().shape({
  username: yup.string().required('You must enter a username'),
  password: yup.string().required('Please enter your password.'),
});

const defaultValues = {
  username: '',
  password: '',
};

function LoginForm() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(({ data }) => data?.error);

  const [errorVisible, setErrorVisible] = useState(false);

  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const handleLogin = ({ username, password }) => {
    const data = { username, password };
    dispatch(actions.wsSend(loginMsg(data)));
  };

  useEffect(() => {
    if (error) {
      setErrorVisible(true);
    } else setErrorVisible(false);
  }, [error]);

  return (
    <div className={classes.root}>
      <form className={classes.loginForm} onSubmit={handleSubmit(handleLogin)}>
        <div className={classes.inputContainer}>
          <Controller
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField
                name="username"
                label="Username *"
                value={value}
                onChange={onChange}
                variant="outlined"
                {...register('username')}
                style={{ width: '100%' }}
                error={errors.username ? true : false}
              />
            )}
            name="username"
            defaultValue={defaultValues.username}
          />

          {errors.username && (
            <p className={classes.errorMessage}> {errors.username.message}</p>
          )}
        </div>
        <div className={classes.inputContainer}>
          <Controller
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField
                name="password"
                label="Password *"
                value={value}
                onChange={onChange}
                variant="outlined"
                type="password"
                {...register('password')}
                style={{ width: '100%' }}
                error={errors.password ? true : false}
              />
            )}
            name="password"
            defaultValue={defaultValues.password}
          />
          {errors.password && (
            <p className={classes.errorMessage}>{errors.password.message}</p>
          )}
        </div>
        <Button type="submit" variant="contained" style={{ marginTop: 20 }}>
          LOGIN
        </Button>
      </form>
      <Toast
        open={errorVisible}
        message={error}
        severity="error"
        onClose={() => {
          dispatch(actions.deleteData('error'));
          setErrorVisible(false);
        }}
      />
    </div>
  );
}

export default LoginForm;
