import React from 'react';
import {LinearProgress, Typography} from '@mui/material';
import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles(() => ({
    root: {
        height: '100vh',
        display: 'flex',
        placeItems: 'center',
        textAlign: 'center',
    },
    loader: {
        width: 330,
        margin: '0 auto',
        display: 'flex',
        flexDirection: 'column',
        padding: 30,
        top: 50,
        position: 'relative',
    },
    items: {
        margin: 10,
    },

}));

const Loader = ({text}) => {
    const classes = useStyles();

    return (<div className={classes.root}>
        <div className={classes.loader}>
            <Typography>{text ?? 'loading...'}</Typography>
            <LinearProgress className="w-xs"/>
        </div>
    </div>);
};

export default Loader;