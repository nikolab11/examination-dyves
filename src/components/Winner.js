import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles, Typography } from '@material-ui/core';
import { actions } from '../socket/actions';
import { bigWinner } from '../socket/socket_messages';

const useStyles = makeStyles(() => ({
  bigWinner: {
    width: 400,
    background: 'white',
    padding: 10,
    marginTop: 10,
    textAlign: 'center',
  },
  list: {
    backgroundColor: '#38414a',
    display: 'flex',
    padding: 10,
    margin: 0,
    flexDirection: 'column',
  },
  row: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  oddRow: {
    backgroundColor: '#2e353e',
  },
  tableText: {
    width: '33%',
    fontWeight: 'lighter',
  },
  white: {
    color: 'white',
  },
  gray: {
    color: '#afb7d0',
  },
  yellow: {
    color: '#ebed08',
  },
}));

const formatMoney = (amount, currency) => {
  return parseFloat(amount / 100).toFixed(Math.abs(2)) + ' ' + currency;
};

const Winner = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const winners = useSelector(({ data }) => data?.winners);

  useEffect(() => {
    dispatch(actions.wsSend(bigWinner()));
  }, [dispatch]);

  return (
    <div className={classes.bigWinner}>
      <Typography variant="h4" gutterBottom component="div">
        Big Winners
      </Typography>
      <div className={classes.list}>
        <div className={`${classes.row}`}>
          <p className={`${classes.tableText} ${classes.white}`}>USERNAME</p>
          <p className={`${classes.tableText} ${classes.white}`}>PAYOUT</p>
        </div>
        {winners?.map(({ amount, currency, player }, index) => {
          const formattedMoney = formatMoney(amount, currency);
          return (
            <div
              key={index}
              className={`${classes.row} ${
                index % 2 === 1 ? classes.oddRow : ''
              }`}
            >
              <p className={`${classes.tableText} ${classes.white}`}>
                {player}
              </p>
              <p className={`${classes.tableText} ${classes.gray}`}>has won</p>
              <p className={`${classes.tableText} ${classes.yellow}`}>
                {formattedMoney}
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Winner;
