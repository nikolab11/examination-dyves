import thunk from 'redux-thunk';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import data from '../reducers/data';
import socketMiddleware from '../socket/socketMiddleware';

const initialState = {};
const reducers = combineReducers({
                                     data,
                                 });
const middleware = [thunk, socketMiddleware];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, initialState, composeEnhancers(applyMiddleware(...middleware)));
export default store;
