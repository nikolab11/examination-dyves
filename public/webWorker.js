let url = null;
let ws = null;
let msgId = 0;
let reconnectCounter = 1;
let waitingTime = 2000;

const wsStates = {
    'OPEN': 1,
    'CLOSING': 2,
    'CLOSED': 3,
    'CONNECTING': 0,
};

onmessage = function (evt) {
    if (!evt.data.task) {
        return;
    }
        switch (evt.data.task) {

        case 'connect':
            if (ws && ws.readyState !== wsStates.CLOSED) {
                if (ws.readyState === wsStates.OPEN) {
                    postMessage({
                                    type: 'socket',
                                    status: 'warning',
                                    msg: 'connection already open',
                                });
                }
                else {
                    postMessage({
                                    type: 'socket',
                                    status: 'warning',
                                    msg: 'not in the right state',
                                });
                }
            }
            else {
                url = evt.data.url;
                socketConnect();
            }
            break;

        case 'disconnect':
            if (!ws || ws.readyState !== wsStates.OPEN) {
                postMessage({
                                type: 'socket',
                                status: 'warning',
                                msg: 'not in the right state',
                            });
            }
            else {
                ws.close();
            }
            break;

        case 'send':
            if (!ws || ws.readyState !== wsStates.OPEN) {
                postMessage({
                                type: 'socket',
                                status: 'warning',
                                msg: 'not open',
                            });
            }
            else {
                if (!evt.data.msg.params) {
                    msgId++;
                    evt.data.msg.params = {
                        'id': msgId,
                        'ts': Date.now(),
                    };
                }
                ws.send(JSON.stringify(evt.data.msg));
            }
            break;

        default:
            console.log(evt.data.task + ' : task not valid');
            break;
        }
};

function wsOnError(evt)
{
    console.log('socket error');

    postMessage({
                    type: 'socket',
                    status: 'error',
                    msg: 'need to reconnect',
                });
}

function wsOnMessage(evt)
{
    postMessage({
                    type: 'protocol',
                    'response': JSON.parse(evt.data),
                });
}

function wsOnClose()
{
    console.log('socket close');
    postMessage({
                    type: 'socket',
                    status: 'close',
                    payload: `Reconnecting in ${waitingTime / 1000} seconds...`,
                    event: 'socketConnectionError',
                });

    ws = null;
    reconnectCounter++;
    setTimeout(() => {
        if (reconnectCounter < 6) {
            console.log(reconnectCounter);
        }
        else {
            waitingTime += 5000;
            reconnectCounter = 1;
        }
        socketConnect();
    }, waitingTime);
}

function wsOnOpen()
{
    console.log('socket open');
    postMessage({
                    type: 'socket',
                    status: 'open',
                    event: '/connect',
                });
}

function socketConnect()
{
    ws = new WebSocket(url);
    ws.onerror = wsOnError;
    ws.onmessage = wsOnMessage;
    ws.onclose = wsOnClose;
    ws.onopen = wsOnOpen;
}